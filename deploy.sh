# Copyright (C) 2023 za267@hotmail.com
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
##########################################################
#!/bin/bash

# Get path
SRC_PATH=$1
DEV_PATH="/home/someguy/minetest_data"

# Copy and Rsync files
#cp ./minetest/bin/minetestserver /usr/local/bin/minetestserver
#cp -a ./minetest/games/minetest_game /usr/local/share/minetest/games/
podman pull registry.gitlab.com/aurelium/aurelium-project/Minetest-Aurelium:latest

# Fix permissions
#chown root:root /usr/local/bin/minetestserver
#chown -R root: /usr/local/share/minetest
chown -R someguy: $DEV_PATH/.minetest/
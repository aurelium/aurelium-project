ARG DOCKER_IMAGE=alpine:3.17
FROM $DOCKER_IMAGE AS builder

ENV MINETEST_GAME_VERSION master
ENV IRRLICHT_VERSION=1.9.0mt10

RUN apk add --no-cache git
RUN mkdir /usr/src &&  git clone --depth 1 https://github.com/minetest/minetest.git -b stable-5 /usr/src/minetest

WORKDIR /usr/src/minetest

RUN apk add --no-cache git build-base cmake sqlite-dev curl-dev zlib-dev zstd-dev \
                gmp-dev jsoncpp-dev postgresql-dev ninja luajit-dev ca-certificates lua-socket irrlicht-dev && \
        #git clone --depth=1 -b ${MINETEST_GAME_VERSION} https://github.com/minetest/minetest_game.git ./games/minetest_game && \
        git clone --depth=1 -b stable-5 https://github.com/minetest/minetest_game.git ./games/minetest_game && \
        rm -fr ./games/minetest_game/.git

WORKDIR /usr/src/
#RUN git clone --recursive https://github.com/jupp0r/prometheus-cpp/ && \
#        mkdir prometheus-cpp/build && \
#        cd prometheus-cpp/build && \
#        cmake .. \
#                -DCMAKE_INSTALL_PREFIX=/usr/local \
#                -DCMAKE_BUILD_TYPE=Release \
#                -DENABLE_TESTING=0 \
#                -GNinja && \
#        ninja && \
#        ninja install

RUN git clone --depth=1 https://github.com/minetest/irrlicht/ -b ${IRRLICHT_VERSION} && \
        cp -r irrlicht/include /usr/include/irrlichtmt

WORKDIR /usr/src/minetest
RUN mkdir build && \
        cd build && \
        cmake .. \
                -DCMAKE_INSTALL_PREFIX=/usr/local \
                -DCMAKE_BUILD_TYPE=Release \
                -DBUILD_SERVER=TRUE \
                -DENABLE_PROMETHEUS=FALSE \
                -DBUILD_UNITTESTS=FALSE \
                -DBUILD_CLIENT=FALSE \
        -DVERSION_EXTRA=Aurelium && \
        make -j4 && \
        make install

ARG DOCKER_IMAGE=alpine:3.17
FROM $DOCKER_IMAGE AS runtime

RUN apk add --no-cache sqlite-libs curl gmp libstdc++ libgcc libpq luajit jsoncpp zstd-libs &&\
        adduser -D minetest --uid 30000 -h /var/lib/minetest && \
        chown -R minetest:minetest /var/lib/minetest

WORKDIR /var/lib/minetest

COPY --from=builder /usr/local/share/minetest /usr/local/share/minetest
COPY --from=builder /usr/local/bin/minetestserver /usr/local/bin/minetestserver
COPY --from=builder /usr/local/share/doc/minetest/minetest.conf.example /etc/minetest/minetest.conf

USER minetest:minetest

EXPOSE 30000/udp 30000/tcp

CMD ["/usr/local/bin/minetestserver", "--config", "/etc/minetest/minetest.conf", "--world", "/var/lib/minetest/.minetest/worlds/world"]

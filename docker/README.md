# Docker

Contains the Dockerfile for building the Minetest image and the docker-compose file
The image is built during CI job.

## Running manually

Run the command `sudo docker-compose up -d` in this directory

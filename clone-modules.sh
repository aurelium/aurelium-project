# Copyright (C) 2023 za267@hotmail.com
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

##########################################################
# This helper script reads a JSON array of module URLs
# and clones them remotely.
# TIP: When generating the modpack submodule list, you
# can simply git clone the modpack, cd into the modpack dir,
# then run the following command to output a nicely formatted
# json array:
# ls -l -d */ | awk '{print "\"" $9 "\": true,"}' | sed 's/\///'
##########################################################
#!/bin/bash

CLONECMD="git clone "
WORLD_MT_MOD_LIST=""
MOD_DIR="mods"

# Prepare mod directories on remote server.
ssh $SSH_OPTIONS $SSH_HOST 'rm -Rf ./mods'
ssh $SSH_OPTIONS $SSH_HOST 'mkdir mods'
ssh $SSH_OPTIONS $SSH_HOST 'cd mods/'
# rm -Rf ./mods
# mkdir mods

# TODO: See if we can use jq's slurp instead.
# GET JSON DATA
JSON=$(cat $1)

# Get Keys from JSON, or fail if invalid json.
# This allows us to test the JSON and fail early.
{
    KEYS=$(jq -r 'keys[]' <<< "$JSON")
} || {
    echo "jq failure:"
    retVal=$?
    # JQ might return an exit code 0 even when parsing fails.  So, lets change that to a 1.
    if [ $retVal -eq 0 ]; then
        echo "Json parsing Error"
        exit 1
    fi
}

# Loop through module list (The git URL is the key).
for KEY in $KEYS; do

    # Get the version and name of the module
    VER=$(jq -r ".[\"$KEY\"].version" <<< "$JSON")
    MODNAME=$(jq -r ".[\"$KEY\"].name" <<< "$JSON")
    MODSTATE=$(jq -r ".[\"$KEY\"].enabled" <<< "$JSON")
    RECURSIVE=$(jq -r ".[\"$KEY\"].recursive" <<< "$JSON")
    DIRNAME=""

    # Get ModPack boolean to test later.
    ModPack=$(jq -r ".[\"$KEY\"].modpack" <<< "$JSON")

    # Print out status.
    echo "Cloning $KEY v $VER NAME: $MODNAME @ $SSH_HOST ModPack: $ModPack"
    # If this is a modpack, loop through submodules.
    if $ModPack; then
        echo "We found a modpack."

        # Retrieve the list of keys from the SubMods array.
        MODPACKS=$(jq -r ".[\"$KEY\"].SubMods | keys[]" <<< "$JSON")
        # Retrieve the submod name and state (enabled/disabled).
        for MPKEY in $MODPACKS; do
            SUBMOD_STATE=$(jq -r ".[\"$KEY\"].SubMods[\"$MPKEY\"]" <<< "$JSON")
            echo "$MPKEY is $SUBMOD_STATE"
            # Add submodule to list of mods in world.mt mods array.
            WORLD_MT_MOD_LIST="${WORLD_MT_MOD_LIST}"$'\n'"load_mod_$MPKEY = $SUBMOD_STATE" 
        done
    else
        # Add code here to populate the world.mt mods array.
        WORLD_MT_MOD_LIST="${WORLD_MT_MOD_LIST}"$'\n'"load_mod_$MODNAME = $MODSTATE"
    fi

    if [[ $RECURSIVE = "true" ]]; then
      RECURS="--recursive "
    else
      RECURS=""
    fi

    # Execute git clone on remote server.
    echo "Will clone using ${CLONECMD} ${RECURS} --branch $VER ${KEY} $MODNAME"
    
    ssh $SSH_OPTIONS $SSH_HOST "cd ~/mods/ && git clone --branch $VER ${KEY} $MODNAME"
done

# CLONE THE PATCHES
ssh $SSH_OPTIONS $SSH_HOST "cd mods && ${CLONECMD} https://gitlab.com/aurelium/aurelium-mods/patches.git"

# GENERATE WORLD.MT FILE
echo "$WORLD_MT_MOD_LIST" > world.mt.mods

ssh $SSH_OPTIONS $SSH_HOST "cd mods && ${CLONECMD} ${RECURS} --branch $VER ${KEY} $MODNAME"

# Upload the modules list file.  The deploy.sh script will use it
# To generate the world.mt file.
scp -P $DEV_PORT ./world.mt.mods $DEV_USER@$DEV_HOST:

# for REPOURL in $REPOLIST; do
#     echo "Cloning $REPOURL @ $SSH_HOST"
#     echo "KEY: $KEYS"
#     # ssh $SSH_OPTIONS $SSH_HOST "cd mods && ${CLONECMD}${REPOURL}"
# done
